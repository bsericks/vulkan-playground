#!/bin/bash

mkdir -p build/shaders
glslc shaders/basic_fragment.frag -o src/shaders/frag.spv
glslc shaders/basic_vertex.vert -o src/shaders/vert.spv

cmake -DCMAKE_BUILD_TYPE=Debug  .
#cmake -DCMAKE_BUILD_TYPE=Release  .
cmake --build .